#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char hashguess[HASH_LEN];
    
    sprintf(hashguess, "%s", md5(guess, strlen(guess)));
    
    if(hash[strlen(hash)-1] == '\n')
    {
        hash[strlen(hash)-1] = '\0';
    }
    
    // Compare the two hashes
    for(int i = 0; i < HASH_LEN; i++ )
    {
        if(hashguess[i] != hash[i])
        {
            return 0;
        }
    }

    return 1;
    // Free any malloc'd memory
    free(hashguess);

}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
int file_length(char *filename)
{
    struct stat info;
    int ret = stat(filename, &info);
    if (ret == -1) return -1;
    else return info.st_size;
}


char **read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    char *pass = malloc(len);
    
    FILE *p = fopen(filename, "r");
    if (!p)
    {
        printf("Can't open %s for reading\n", filename);
        exit(1);
    }
    fread(pass, sizeof(char), len, p);
    fclose(p);
    
    int num_passes = 0;
    for (int i = 0; i < len; i++)
    {
        if (pass[i] == '\n')
        {
            pass[i] = '\0';
            num_passes++;
        }
    }
    
    char **passes = malloc(num_passes * sizeof(char *));
    
    passes[0] = &pass[0];
    int j = 1;
    for(int i = 0; i < len-1; i++)
    {
        if(pass[i] == '\0' )
        {
            passes[j] = &pass[i+1];
            j++;
        }
    }
    int *dl = &num_passes;
    *size = *dl;
    return passes;
    fclose(p);
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int *dlen = (int *)malloc(sizeof(int));
    char **dict = read_dictionary(argv[2], dlen);
    
    // Open the hash file for reading.
    FILE* fpin;
    fpin = fopen(argv[1], "r");
    if (!fpin)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int count = 0;
    char line[100];
    while(fgets(line, 100, fpin) != NULL)
    {
        for(int i = 0; i < *dlen; i++)
        {
            if(tryguess(line, dict[i]) == 1)
            {
                printf("%s: %s\n", dict[i], line);
                count++;
            }
        }
    }
    printf("%d passwords found.\n", count);
    fclose(fpin);
}